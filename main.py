# Work with Python 3.6
import discord
import discord.utils
from discord.ext import commands
from discord.utils import get

import os

TOKEN = 'NTk4NjgzNjQ1Mjg4NTEzNTUz.XSaNmA.u93DCktfgUyTTkgZB16Br04WumU'


client = discord.Client()
bot = commands.Bot(command_prefix='$')

@bot.command(name='raid')
async def raid(ctx):
    guild = ctx.guild
    member = ctx.author
    admin_role = get(guild.roles, name="Admin")
    overwrites = {
        guild.default_role: discord.PermissionOverwrite(read_messages=False),
        guild.me: discord.PermissionOverwrite(read_messages=True),
        admin_role: discord.PermissionOverwrite(read_messages=True)
    }
    channel = await guild.create_text_channel('secret', overwrites=overwrites)

@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return
    if "!raid" in message.content:
        make_channel(client)
    if "Want to raid?" in message.content:
        channel = discord.utils.get(client.guilds, name="general")
        msg = 'Hello {0.author.mention}, did you know you can post a screenshot in {0.channel.mention} to start a raid?'.format(message)
        print(message.channel.id)
        await message.channel.send(msg)

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

client.run(TOKEN)
